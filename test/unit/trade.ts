import {expect} from "chai";
import "mocha";
import {GameData, StarshipStatus} from "../../src/app/logic/types";
import {getTotalCost, TradeValidationErrors, validateTrade} from "../../src/app/logic/trade";

const initialData: GameData = {
    credits: 100,
    items: ["Item", "Sth"],
    planets: {
        "1": {
            available_items: {
                "Item": {
                    available: 2,
                    sell_price: 10,
                    buy_price: 20
                },
                "Sth": {
                    available: 3,
                    sell_price: 20,
                    buy_price: 30
                }
            },
            x: 0,
            y: 0
        },
        "2": {
            available_items: {
                "Item": {
                    available: 0,
                    sell_price: 20,
                    buy_price: 30
                }
            },
            x: 0,
            y: 10
        }
    },
    starships: {
        "Starship": {
            cargo_hold_size: 1,
            origin: "1",
            cargo: {},
            status: StarshipStatus.Docked,
            destination: "",
            travel_time: 0,
            time_left: 0
        }
    },
    time: 10
};

const planet1 = initialData.planets["1"];
const planet2 = initialData.planets["2"];
const starship = initialData.starships["Starship"];

const noTradeErrors: TradeValidationErrors = {
    cargo: null, credits: null, planet: {}, sell: {}, valid: true
};


describe("validateTrade", () => {
    it("should return no errors when you buy and it's correct", () => {
        expect(validateTrade(planet1, starship, planet1.available_items["Item"].buy_price, {"Item": 1})).to.eql(noTradeErrors);
    });

    it("should return errors when you buy and don't have enough credits", () => {
        let credits = 4;
        expect(validateTrade(planet1, starship, credits, {"Item": 1})).to.eql({
            cargo: null,
            credits: {after: credits - planet1.available_items["Item"].buy_price, credits: credits},
            planet: {},
            sell: {},
            valid: false
        });
    });
});

describe("getTotalCost", () => {
    it("should return 0 when shopping cart is empty", () => {
        expect(getTotalCost(planet1, {})).to.eql(0);
    });

    it("should properly compute total cost", () => {
        let itemCount = 2;
        let sthCount = 3;
        expect(getTotalCost(planet1, {
            "Item": itemCount,
            "Sth": -sthCount
        })).to.eql(planet1.available_items["Item"].buy_price * -itemCount + planet1.available_items["Sth"].sell_price * sthCount);
    });
});