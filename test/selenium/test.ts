import {Builder, By, Key, until} from 'selenium-webdriver';
import {Options} from "selenium-webdriver/chrome";
import {Server} from "http";
import {Express} from "express";
import * as path from "path";
import {expect} from "chai";
import "mocha";
import express = require('express');

const options = new Options().addArguments('--allow-file-access-from-files',
    '--disable-web-security', '--user-data-dir=.chrome');

const driver = new Builder()
    .forBrowser('chrome')
    .setChromeOptions(options).build();

describe('DefaultTest', () => {

    let app: Express;
    let server: Server;

    before(() => {
        app = express();
        console.log(__dirname);
        app.use(express.static(path.join(__dirname, '../../dist')));
        server = app.listen(5000, () => console.log("listening..."));
    });

    it('should start a new game and play for a while', async function () {
        let name = 'selenium';
        this.timeout(20000);
        await driver.get('localhost:5000/start.html');
        await driver.wait(until.elementLocated(By.className('start')));
        await driver.findElement(By.linkText("Play!")).click();
        await driver.findElement(By.name('name')).sendKeys(name, Key.ENTER);
        await driver.wait(until.elementLocated(By.className('game')));
        expect(await driver.findElement(By.id('name')).getText()).to.eql(name);
        await driver.findElement(By.linkText("Axiom")).click();
        await driver.wait(until.elementLocated(By.className('details-container')));
        await driver.findElement(By.name("Cynamon")).sendKeys(10);
        await driver.findElement(By.id("submit-trade")).click();
        expect(await driver.findElement(By.id('credits')).getText()).to.eql('1874');
    });

    after(async () => {
        driver.quit();
        server.close();
    });
});