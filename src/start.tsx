import {h, render} from 'preact';
import {Start} from "./app/rendering/start";

require('preact/debug');

let appElement = document.getElementById('start');
if (appElement) {
    render(<Start/>, appElement);
}
