import {h, render} from 'preact';
import {App} from './app/rendering/app';

require('preact/debug');

let appElement = document.getElementById('app');
if (appElement) {
    render(<App/>, appElement);
}
