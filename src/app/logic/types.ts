export type AvailableItems = { [name: string]: AvailableItemData };

export interface AvailableItemData {
    available: number;
    buy_price: number;
    sell_price: number;
}

export type Planets = { [name: string]: PlanetData }

export interface PlanetData {
    available_items: AvailableItems;
    x: number;
    y: number;
}

export enum StarshipStatus {
    EnRoute = "En route",
    Docked = "Docked"
}

export type Cargo = { [name: string]: number };

export type Starships = { [name: string]: StarshipData }

export interface StarshipData {
    cargo_hold_size: number;
    cargo: Cargo;
    status: StarshipStatus;
    origin: string;
    destination: string;
    time_left: number;
    travel_time: number;
}

export interface GameData {
    time: number;
    credits: number;
    items: string[];
    planets: Planets;
    starships: Starships;
}

export interface PresetDetails {
    id: number;
    name: string;
    author: string;
    created: number;
    description: string;
}