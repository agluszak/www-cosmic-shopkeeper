import {Cargo, PlanetData, StarshipData} from "./types";

export interface CargoHoldError {
    total: number;
    max: number;
}

function cargoHoldError(total: number, max: number): CargoHoldError {
    return {total: total, max: max};
}

export interface NotEnoughOnPlanetError {
    maxCount: number;
    requested: number;
}

function notEnoughOnPlanetError(maxCount: number, requested: number): NotEnoughOnPlanetError {
    return {maxCount: maxCount, requested: requested};
}

export interface NotEnoughCreditsError {
    after: number;
    credits: number;
}

function notEnoughCreditsError(after: number, credits: number): NotEnoughCreditsError {
    return {after: after, credits: credits};
}

export interface NotEnoughItemsToSellError {
    maxCount: number;
    requested: number;
}

function notEnoughItemsToSellError(maxCount: number, requested: number): NotEnoughItemsToSellError {
    return {maxCount: maxCount, requested: requested};
}

export interface TradeValidationErrors {
    cargo: CargoHoldError | null;
    credits: NotEnoughCreditsError | null;
    planet: { [name: string]: NotEnoughOnPlanetError };
    sell: { [name: string]: NotEnoughItemsToSellError };
    valid: boolean;
}

export function getCost(count: number, buy_price: number, sell_price: number) {
    if (count > 0) {
        return buy_price * -count;
    } else {
        return sell_price * -count;
    }
}

export function getTotalCost(planet: PlanetData, items: Cargo) {
    return Object.entries(items)
        .map(([item, count]) =>
            count != 0
                ? getCost(count, planet.available_items[item].buy_price, planet.available_items[item].sell_price)
                : 0)
        .reduce((a, b) => a + b, 0);
}

export function validateTrade(planet: PlanetData, starship: StarshipData, credits: number, items: Cargo): TradeValidationErrors {
    let errors: TradeValidationErrors = {
        credits: null,
        cargo: null,
        planet: {},
        sell: {},
        valid: true
    };

    let itemsBought = Object.entries(items)
        .filter(([item, count]) => count > 0);
    let itemsSold = Object.entries(items)
        .filter(([item, count]) => count < 0)
        .map(([item, count]) => [item, -count] as [string, number]);

    itemsBought
        .filter(([item, count]) => count > planet.available_items[item].available)
        .forEach(([item, count]) => errors.planet[item] = notEnoughOnPlanetError(planet.available_items[item].available, count));

    if (Object.values(errors.planet).length != 0) {
        errors.valid = false;
    }

    itemsSold
        .filter(([item, count]) => starship.cargo[item] ? count > starship.cargo[item] : true)
        .forEach(([item, count]) => errors.sell[item] = notEnoughItemsToSellError(starship.cargo[item], count));

    if (Object.values(errors.sell).length != 0) {
        errors.valid = false;
    }

    let currentCargoSize = Object.values(starship.cargo).reduce((a, b) => a + b, 0);
    let changeInOccupiedSpace = Object.values(items).reduce((a, b) => a + b, 0);

    if (currentCargoSize + changeInOccupiedSpace > starship.cargo_hold_size) {
        errors.cargo = cargoHoldError(currentCargoSize + changeInOccupiedSpace, starship.cargo_hold_size);
        errors.valid = false;
    }

    let totalCost = getTotalCost(planet, items);

    if (credits + totalCost < 0) {
        errors.credits = notEnoughCreditsError(credits + totalCost, credits);
        errors.valid = false;
    }

    return errors;
}