let reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
let reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;

export function dateParser(key: string, value: string): Date | any {
    let a = reISO.exec(value);
    if (a)
        return new Date(value);
    a = reMsAjax.exec(value);
    if (a) {
        let b = a[1].split(/[-+,.]/);
        return new Date(b[0] ? +b[0] : 0 - +b[1]);
    }
    return value;
}

export function getURLParameter(sParam: string): string | undefined {
    let sPageURL = window.location.search.substring(1);
    let sURLVariables = sPageURL.split('&');
    for (let i = 0; i < sURLVariables.length; i++) {
        let sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return decodeURIComponent(sParameterName[1].replace(/\+/g, '%20'));
        }
    }
    return undefined;
}