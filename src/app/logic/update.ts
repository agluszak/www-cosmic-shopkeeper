import {Cargo, GameData, StarshipStatus} from "./types";

export function timeUpdate(data: GameData): GameData {
    let time = data.time > 0 ? data.time - 1 : 0;
    let newState = JSON.parse(JSON.stringify(data)) as GameData;
    newState.time = time;
    Object.keys(newState.starships).forEach((name) => {
        if (newState.starships[name].status == StarshipStatus.EnRoute) {
            if (newState.starships[name].time_left > 0) {
                newState.starships[name].time_left -= 1;
            } else {
                newState.starships[name].origin = newState.starships[name].destination;
                newState.starships[name].status = StarshipStatus.Docked;
                newState.starships[name].time_left = 0;
            }
        }
    });

    return newState;
}

export function tradeUpdate(data: GameData, items: Cargo, total: number, starshipName: string): GameData {
    let newState = JSON.parse(JSON.stringify(data)) as GameData;

    newState.credits += total;

    let starship = newState.starships[starshipName];
    Object.entries(items).forEach(([name, count]) => {
            if (!starship.cargo[name]) {
                starship.cargo[name] = 0;
            }
            starship.cargo[name] += count;
            if (newState.planets[starship.origin].available_items[name]) {
                newState.planets[starship.origin].available_items[name].available -= count;
            }
        }
    );
    return newState;
}

export function travelUpdate(data: GameData, destination: string, eta: number, starshipName: string) {
    let newState = JSON.parse(JSON.stringify(data)) as GameData;

    newState.starships[starshipName].destination = destination;
    newState.starships[starshipName].time_left = eta;
    newState.starships[starshipName].travel_time = eta;
    newState.starships[starshipName].status = StarshipStatus.EnRoute;

    return newState;

}