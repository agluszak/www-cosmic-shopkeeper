export type Scores = { [name: string]: number }

let names = ["Mieczysław Broniarz", "Zbigniew Dzikowski",
    "Krzysztof Ciebiera", "Krzysztof Diks",
    "Eryk Kopczyński", "Pies Husky",
    "Joanna Schoering-Wielgus", "Maria Jezus"];

function shuffle<T>(a: T[]) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

export function generateScores(count: number, maxScore: number): Scores {
    let scores = {} as Scores;
    shuffle(names);
    for (let i = 0; i < count; i++) {
        let score = Math.floor(Math.random() * maxScore);
        let name = names[i];
        scores[name] = score;
    }
    return scores;
}

export function addScore(scores: Scores, name: string, score: number): Scores {
    let newScores = JSON.parse(JSON.stringify(scores));
    newScores[name] = score;
    return newScores;
}