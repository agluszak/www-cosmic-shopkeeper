import {StarshipData, Starships, StarshipStatus} from "../../logic/types";
import {h} from "preact";

function renderStarship(name: string, data: StarshipData, clickHandler: (name: string) => void) {
    return (data.status == StarshipStatus.EnRoute) ? <li>
        <a class="en-route" href="#popup" onClick={(e) => clickHandler(name)}>{name}</a>
        <span class="starship-info">({data.time_left})</span>
    </li> : <li>
        <a href="#popup" onClick={(e) => clickHandler(name)}>{name}</a>
        <span class="starship-info">(<a href="#popup">{data.origin}</a>)</span>
    </li>
}

type StarshipsProps = { starships: Starships, clickHandler: (name: string) => void };
export const StarshipsComponent = (props: StarshipsProps) => {
    if (!props.starships) {
        return <p>Wczytywanie</p>
    }
    let list = Object.entries(props.starships)
        .map(([name, data]) => renderStarship(name, data, props.clickHandler));
    return <ul> {list} </ul>;
};