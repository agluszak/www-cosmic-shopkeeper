import {Component, h} from "preact";
import {PlanetData, Planets, StarshipData} from "../../logic/types";

export type TravelProps = {
    planets: Planets, starship: StarshipData, travelSubmit: (destination: string, eta: number) => void
}

export type TravelState = {
    destination: string,
    eta: number
}

function getTravelTime(origin: PlanetData, destination: PlanetData) {
    let xDiff = origin.x - destination.x;
    let yDiff = origin.y - destination.y;
    return Math.ceil(Math.sqrt(xDiff * xDiff + yDiff * yDiff));
}

export class TravelComponent extends Component<TravelProps, TravelState> {

    constructor(props: TravelProps) {
        super(props);
        let destinationName = Object.keys(props.planets)[0];
        let destination = props.planets[destinationName];
        let origin = props.planets[props.starship.origin];
        this.state = {
            destination: destinationName,
            eta: getTravelTime(origin, destination)
        }
    }

    // TODO Make it impossible to select current origin as destination
    render(props: TravelProps, state: TravelState) {
        return <div class="starship-travel-container">
            <form onSubmit={(e) => props.travelSubmit(this.state.destination, this.state.eta)} action="javascript:">
                <label class="strong" for="destination">Destination:</label>
                <select name="destination" value={this.state.destination} onChange={(e) => this.updateDestination(e)}>
                    {Object.keys(props.planets).map((planet) =>
                        <option value={planet}>{planet}</option>
                    )}
                </select>
                <p class="detail">
                    <span class="strong">Time estimate: </span>
                    <span id="time-estimate">{state.eta}</span>
                </p>
                <a href="#"><input type="submit" value="Travel!" class="btn"/></a>
            </form>
        </div>
    }

    private updateDestination(e: Event) {
        const target: any = e.target;
        const value = target.value;
        let newState = JSON.parse(JSON.stringify(this.state)) as TravelState;
        newState.destination = value;
        let origin = this.props.planets[this.props.starship.origin];
        let destination = this.props.planets[value];
        newState.eta = getTravelTime(origin, destination);
        this.setState(newState);
    }
}

