import {Cargo, Planets, Starships, StarshipStatus} from "../../logic/types";
import {h} from "preact";
import {renderClickable} from "../app";
import {MarketComponent} from "./market";
import {TravelComponent} from "./travel";

export enum PopupType {None, Planet, Starship}

export type PopupState = { popupType: PopupType; name: string }

export type PopupProps = {
    state: PopupState, starships: Starships, planets: Planets, planetClick: (name: string) => void,
    starshipClick: (name: string) => void, credits: number, items: string[],
    marketSubmit: (items: Cargo, credits: number, starship: string) => void,
    travelSubmit: (destination: string, eta: number, starship: string) => void;
}

export const Popup = (props: PopupProps) => {
    if (props.state.popupType == PopupType.None) {
        return <span>Loading</span>
    } else if (props.state.popupType == PopupType.Starship) {
        let starshipName = props.state.name;
        let starshipData = props.starships[starshipName];
        let planetData = props.planets[starshipData.origin];
        return <div class="details-container">
            <h3>{starshipName}</h3>
            <div className={starshipData.status == StarshipStatus.Docked ? "starship-status-container" : ""}>
                <p class="detail">
                    <span class="strong">Current status:</span>
                    {starshipData.status == StarshipStatus.EnRoute &&
                    <span>En route ({renderClickable(starshipData.origin, props.planetClick)}) -> ({renderClickable(starshipData.destination, props.planetClick)})</span>}
                    {starshipData.status == StarshipStatus.Docked &&
                    <span>Docked ({renderClickable(starshipData.origin, props.planetClick)})</span>}
                </p>
                {starshipData.status == StarshipStatus.EnRoute &&
                <p class="detail"><span class="strong">Time left: </span><span>{starshipData.time_left}</span></p>}
            </div>
            {starshipData.status == StarshipStatus.Docked &&
            <TravelComponent planets={props.planets} starship={starshipData}
                             travelSubmit={(destination, eta) => props.travelSubmit(destination, eta, props.state.name)}/>}
            <p>
                <span class="strong">Cargo:</span>
            </p>
            <table>
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Quantity</th>
                </tr>
                </thead>
                <tbody>
                {Object.entries(starshipData.cargo)
                    .filter(([item, count]) => count > 0)
                    .map(([item, count]) =>
                        <tr>
                            <td>{item}</td>
                            <td>{count}</td>
                        </tr>
                    )}
                </tbody>
            </table>
            <p class="detail">
                <span class="strong">Total:</span>
                <span>{Object.values(starshipData.cargo).reduce((a, b) => a + b, 0)}</span>
                <span>/</span>
                <span>{starshipData.cargo_hold_size}</span>
            </p>
            {starshipData.status == StarshipStatus.Docked &&
            <MarketComponent planet={planetData} credits={props.credits} starship={starshipData}
                             submitHandler={((items, credits) => props.marketSubmit(items, credits, props.state.name))}
                             items={props.items}/>}
        </div>;
    } else {
        let planetName = props.state.name;
        let planetData = props.planets[planetName];
        return <div><h3><span>{planetName}</span></h3>
            <p class="detail">
                <span class="strong">Position:</span>
                <span id="position">({planetData.x}, {planetData.y})</span>
            </p>
            <p>
                <span class="strong">Market:</span>
            </p>
            <table>
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Selling price</th>
                    <th>Buying price</th>
                </tr>
                </thead>
                <tbody>
                {Object.entries(planetData.available_items).map(([name, item]) =>
                    <tr>
                        <td>{name}</td>
                        <td>{item.available}</td>
                        <td>{item.sell_price}$</td>
                        <td>{item.buy_price}$</td>
                    </tr>
                )}
                </tbody>
            </table>
            <p class="detail"><span class="strong">Docked starships:</span></p>
            {Object.entries(props.starships)
                .filter(([name, data]) => data.status == StarshipStatus.Docked && data.origin == planetName)
                .map(([name, data]) =>
                    renderClickable(name, props.starshipClick)
                )}
        </div>

    }
};