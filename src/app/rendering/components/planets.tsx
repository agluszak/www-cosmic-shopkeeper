import {h} from "preact";
import {renderClickable} from "../app";
import {Planets} from "../../logic/types";

type PlanetsProps = { planets: Planets, clickHandler: (name: string) => void };

export const PlanetsComponent = (props: PlanetsProps) => {
    if (!props.planets) {
        return <p>Wczytywanie</p>
    }
    let list = Object.keys(props.planets)
        .map((name) => <li>{renderClickable(name, props.clickHandler)}</li>);
    return <ul> {list} </ul>;
};
