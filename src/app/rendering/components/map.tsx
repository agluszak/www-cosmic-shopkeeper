import {Component, h} from "preact";
import {Planets, Starships, StarshipStatus} from "../../logic/types";

export type MapProps = {
    planets: Planets, starships: Starships,
    planetClick: (name: string) => void,
    starshipClick: (name: string) => void
}

type MapState = {}

export class MapComponent extends Component<MapProps, MapState> {

    render(props: MapProps, state: MapState) {
        return <svg viewBox="0 0 100 100">
            {Object.entries(props.planets).map(([name, data]) =>
                [<circle onClick={(e) => props.planetClick(name)} cx={data.x} cy={data.y} r="2" stroke="green"
                         stroke-width="1" fill="yellow"/>,
                    <text x={data.x} y={data.y + 4} fill="white" text-anchor="middle" font-size="2.5">{name}</text>]
            )}
            {Object.entries(props.starships).filter(([name, data]) => data.status == StarshipStatus.EnRoute).map(([name, data]) => {
                    let x1 = props.planets[data.origin].x;
                    let y1 = props.planets[data.origin].y;
                    let x2 = props.planets[data.destination].x;
                    let y2 = props.planets[data.destination].y;

                    let starshipX = x2 + data.time_left * (x1 - x2) / data.travel_time;
                    let starshipY = y2 + data.time_left * (y1 - y2) / data.travel_time;

                    return [<line x1={x1} y1={y1} x2={x2} y2={y2} style="stroke:blue;stroke-width:0.5"
                                  stroke-dasharray="2, 2"/>,
                        <circle onClick={(e) => props.starshipClick(name)} cx={starshipX} cy={starshipY} r="1" stroke="red"
                                stroke-width="0.75" fill="lightblue"/>,
                        <text x={starshipX} y={starshipY + 3} fill="white" text-anchor="middle" font-size="2">{name}</text>
                    ]
                }
            )}
        </svg>
    }
}