import {Cargo, PlanetData, StarshipData} from "../../logic/types";
import {getCost, getTotalCost, TradeValidationErrors, validateTrade} from "../../logic/trade";
import {Component, h} from "preact";

export type MarketProps = { planet: PlanetData, credits: number, starship: StarshipData, items: string[], submitHandler: (items: Cargo, credits: number) => void }
export type MarketState = { items: Cargo, total: number, errors: TradeValidationErrors }

let initialState: MarketState = {
    items: {}, total: 0, errors: {
        credits: null,
        cargo: null,
        planet: {},
        sell: {},
        valid: true
    }
};

export class MarketComponent extends Component<MarketProps, MarketState> {

    constructor(props: MarketProps) {
        super(props);
        this.clearForm(props);
    }

    render(props: MarketProps, state: MarketState) {
        return <div>
            <p><span class="strong">Market:</span></p>
            <form onSubmit={(e) => {
                props.submitHandler(this.state.items, this.state.total);
                this.clearForm(props)
            }} action="javascript:">
                <table class="responsive-table">
                    <thead>
                    <tr>
                        <th scope="col">Item</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Selling price</th>
                        <th scope="col">Buying price</th>
                        <th scope="col">Buy/Sell</th>
                        <th scope="col">$</th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.entries(props.planet.available_items).map(([name, item]) =>
                        <tr className={this.state.errors.planet[name] || this.state.errors.sell[name] ? 'form-error' : ''}>
                            <td data-label="Item">{name}</td>
                            <td data-label="Quantity">{item.available}</td>
                            <td data-label="Selling price">{item.sell_price}</td>
                            <td data-label="Buying price">{item.buy_price}</td>
                            <td data-label="Buy/Sell"><input type="number" name={name}
                                                             onInput={(e) => this.updateCount(e)}
                                                             value={this.state.items[name]}/></td>
                            <td data-label="$">{getCost(state.items[name], item.buy_price, item.sell_price)}$</td>
                        </tr>
                    )}

                    </tbody>
                </table>
                <div class="apply-trade-container">
                    <p className={"margin-right " + (this.state.errors.cargo ? 'form-error' : '')}>
                        <span class="strong">Free space:</span>
                        <span>{props.starship.cargo_hold_size - Object.values(props.starship.cargo).reduce((a, b) => a + b, 0)}</span>
                    </p>
                    <p class="margin-right">
                        <span class="strong">Credits:</span>
                        <span>{props.credits}$</span>
                    </p>
                    <input class="btn margin-right" type="submit" value="Trade!" id="submit-trade"
                           disabled={!state.errors.valid}/>
                    <p className={this.state.errors.credits ? 'form-error' : ''}>
                        <span class="strong">Total:</span>
                        <span id="total">{state.total > 0 && "+"}{state.total}$</span>
                    </p>
                </div>
            </form>
        </div>
    }

    private clearForm(props: MarketProps) {
        let newState = initialState;
        props.items.forEach((name) =>
            newState.items[name] = 0
        );
        this.setState(newState);
    }

    private updateCount(e: Event) {
        const target: any = e.target;
        const value = parseInt(target.value);
        const name = target.name;
        let newState = JSON.parse(JSON.stringify(this.state)) as MarketState;
        newState.items[name] = value;
        newState.total = getTotalCost(this.props.planet, newState.items);
        newState.errors = validateTrade(this.props.planet, this.props.starship, this.props.credits, newState.items);

        this.setState(newState);
    }


}
