import {Component, h} from 'preact';
import {dateParser} from "../logic/utils";
import {Cargo, GameData, StarshipStatus} from "../logic/types"
import {Popup, PopupState, PopupType} from "./components/popup";
import {timeUpdate, tradeUpdate, travelUpdate} from "../logic/update";
import {StarshipsComponent} from "./components/starships"
import {PlanetsComponent} from "./components/planets"
import {MapComponent} from "./components/map";
import {addScore, Scores} from "../logic/scores";

type NoProps = {};

export function renderClickable(name: string, clickHandler: (name: string) => void) {
    return <a href="#popup" onClick={(e) => clickHandler(name)}>{name}</a>
}

type AppState = { data: GameData, popup: PopupState, player_name: string }

export class App extends Component<NoProps, AppState> {

    constructor() {
        super();
    }

    async componentDidMount() {
        let preset = sessionStorage.getItem('preset');
        let data: GameData;
        if (!preset) {
            let response = await fetch('initial_data.json');
            let text = await response.text();
            data = await JSON.parse(text, dateParser) as GameData;
        } else {
            data = JSON.parse(JSON.parse(preset));
        }

        let player_name_from_url = sessionStorage.getItem('playerName');
        let player_name;
        if (player_name_from_url) {
            player_name = player_name_from_url;
        } else {
            player_name = "Player";
        }
        Object.keys(data.starships).forEach((starship) => {
                data.starships[starship].cargo = {};
                data.starships[starship].status = StarshipStatus.Docked;
            }
        );
        this.setState({popup: {popupType: PopupType.None, name: "undefined"}, data, player_name});


        const update = () => {
            this.setState({popup: this.state.popup, data: timeUpdate(this.state.data)});
            if (this.state.data.time > 0) {
                setTimeout(update, 1000);
            } else {
                this.handleFinishGame();
            }
        };

        update();
    }

    render(props: NoProps, state: AppState) {
        if (!state || !state.data) {
            return <span>Loading</span>
        }
        return <div>
            <div class="container game">
                <header class="header">
                    <h1>Cosmic Shopkeeper!</h1>
                    <h3 id="welcome">Welcome, <span class="strong" id="name">{state.player_name}</span></h3>
                    <p class="stat">
                        <span class="strong">Credits: </span>
                        <span id="credits">{state.data.credits}</span>
                    </p>
                    <p class="stat">
                        <span class="strong">Time: </span>
                        <span id="time">{state.data.time}</span>
                    </p>
                </header>
                <nav class="ships">
                    <p class="strong">Starships: </p>
                    <StarshipsComponent starships={state.data.starships}
                                        clickHandler={(name) => this.handleStarshipClick(name)}/>
                </nav>
                <main class="map">
                    {/*<img alt="Map" src="space.jpg"/>*/}
                    <MapComponent
                        planetClick={(name) => this.handlePlanetClick(name)}
                        starshipClick={(name) => this.handleStarshipClick(name)}
                        planets={state.data.planets} starships={state.data.starships}/>
                </main>
                <aside class="planets">
                    <p class="strong">Planets: </p>
                    <PlanetsComponent planets={state.data.planets}
                                      clickHandler={(name) => this.handlePlanetClick(name)}/>
                </aside>
                <footer class="footer">
                    <p>Van Binh Studios 2019</p>
                </footer>
            </div>

            <div class="popup stretch-on-mobile" id="popup">
                <a class="close" href="#">&times;</a>
                <div class="details-container">
                    <Popup state={state.popup} planets={state.data.planets} starships={state.data.starships}
                           planetClick={(name) => this.handlePlanetClick(name)}
                           starshipClick={(name) => this.handleStarshipClick(name)} credits={state.data.credits}
                           items={state.data.items}
                           marketSubmit={(items, credits, starship) => this.handleMarketSubmit(items, credits, starship)}
                           travelSubmit={(destination, eta, starship) => this.handleTravelSubmit(destination, eta, starship)}/>
                </div>
            </div>
            <a class="close-popup" href="#"></a>
        </div>
    }

    private handlePlanetClick(name: string) {
        window.location.hash = "popup";
        let newPopupState = JSON.parse(JSON.stringify(this.state.popup)) as PopupState;
        newPopupState.popupType = PopupType.Planet;
        newPopupState.name = name;
        this.setState({popup: newPopupState, data: this.state.data});
    }

    private handleStarshipClick(name: string) {
        window.location.hash = "popup";
        let newPopupState = JSON.parse(JSON.stringify(this.state.popup)) as PopupState;
        newPopupState.popupType = PopupType.Starship;
        newPopupState.name = name;
        this.setState({popup: newPopupState, data: this.state.data});
    }

    private handleMarketSubmit(items: Cargo, credits: number, starship: string) {
        this.setState({popup: this.state.popup, data: tradeUpdate(this.state.data, items, credits, starship)});
    }

    private handleTravelSubmit(destination: string, eta: number, starship: string) {
        window.location.hash = "";
        this.setState({popup: this.state.popup, data: travelUpdate(this.state.data, destination, eta, starship)});
    }

    private handleFinishGame() {
        let scoresString = localStorage.getItem("scores");
        if (scoresString) {
            let scores = JSON.parse(scoresString) as Scores;
            let newScores = addScore(scores, this.state.player_name, this.state.data.credits);
            localStorage.setItem("scores", JSON.stringify(newScores));
        }
        window.location.pathname = '/start.html'
    }
}