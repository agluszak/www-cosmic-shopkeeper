import {Component, h} from "preact";
import {generateScores, Scores} from "../logic/scores";
import {PresetDetails} from "../logic/types";

type StartProps = {};

type StartState = {
    scores: Scores;
    login: LoginParams;
    loggedIn: boolean;
    text: string;
    preset: PresetParams;
    presetDetails: PresetDetails[];
    selectedPreset: PresetDetails | null;
    playerName: string;
};

type PresetParams = {
    content: string;
    name: string;
    description: string;
}

type LoginParams = {
    username: string;
    password: string;
}


export class Start extends Component<StartProps, StartState> {

    constructor(props: StartProps) {
        super(props);
        let scoresString = localStorage.getItem("scores");
        let scores: Scores;
        if (!scoresString) {
            scores = generateScores(5, 5000);
        } else {
            scores = JSON.parse(scoresString) as Scores;
        }
        localStorage.setItem("scores", JSON.stringify(scores));
        this.state = {
            scores, login: {username: "", password: ""}, loggedIn: false, text: "",
            preset: {name: "", description: "", content: ""}, presetDetails: [],
            selectedPreset: null, playerName: ""
        };
    }

    async componentDidMount() {
        await this.handleFetch("http://localhost:3031/presets/list", {
            method: 'GET',
        }, async (ok, state) => {
            state.presetDetails = ok;
        });
    }

    updateUsername(e: Event) {
        const target: any = e.target;
        let newState = Object.assign({}, this.state) as StartState;
        newState.login.username = target.value;
        this.setState(newState);
    }

    updatePresetName(e: Event) {
        const target: any = e.target;
        let newState = Object.assign({}, this.state) as StartState;
        newState.preset.name = target.value;
        this.setState(newState);
    }

    updatePlayerName(e: Event) {
        const target: any = e.target;
        let newState = Object.assign({}, this.state) as StartState;
        newState.playerName = target.value;
        this.setState(newState);
    }

    updatePresetDescription(e: Event) {
        const target: any = e.target;
        let newState = Object.assign({}, this.state) as StartState;
        newState.preset.description = target.value;
        this.setState(newState);
    }

    updatePassword(e: Event) {
        const target: any = e.target;
        let newState = Object.assign({}, this.state) as StartState;
        newState.login.password = target.value;
        this.setState(newState);
    }

    changeSelectedPreset(e: Event) {
        const target: any = e.target;
        let newState = Object.assign({}, this.state) as StartState;
        let selected = newState.presetDetails.find(preset => preset.name == target.value);
        newState.selectedPreset = selected == undefined ? null : selected;
        this.setState(newState);
    }

    async selectFile(e: Event) {
        const target: any = e.target;
        let newState = Object.assign({}, this.state) as StartState;
        newState.preset.content = await new Response(target.files[0]).text();
        this.setState(newState);
    }

    async submitPreset() {
        await this.handleFetch("http://localhost:3031/presets/upload", {
            method: 'POST',
            credentials: 'include',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(this.state.preset)
        }, async (ok, state) => {
            state.text = ok;
        }, async (error, state) => {
            state.text = error;
        });

        await this.handleFetch("http://localhost:3031/presets/list", {
            method: 'GET',
        }, async (ok, state) => {
            state.presetDetails = ok;
        });
    }

    async play() {
        sessionStorage.setItem('playerName', this.state.playerName);
        if (this.state.selectedPreset) {
            await this.handleFetch("http://localhost:3031/presets/get/" + this.state.selectedPreset.name, {
                method: 'GET'
            }, async (ok, state) => {
                sessionStorage.setItem('preset', ok.content)
            }, async (error, state) => {
                state.text = error;
            });
        }
        console.log("asd");
        window.location.href = "http://localhost:3030/index.html";

    }

    async handleFetch(url: RequestInfo, params: RequestInit,
                      onSuccess: (ok: any, state: StartState) => Promise<void> = () => Promise.resolve(),
                      onFailure: (error: any, state: StartState) => Promise<void> = () => Promise.resolve()): Promise<void> {
        let newState = Object.assign({}, this.state) as StartState;

        try {
            let response = await fetch(url, params);
            let body = await response.json();

            if (response.ok && body.ok) {
                await onSuccess(body.ok, newState);
            } else if (body.error) {
                await onFailure(body.error, newState);
            } else {
                console.log(body);
            }
        } catch (e) {
            console.log(e);
        }
        this.setState(newState);
    }

    async login() {
        await this.handleFetch("http://localhost:3031/users/login", {
            method: 'POST',
            credentials: 'include',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(this.state.login)
        }, async (ok, state) => {
            state.loggedIn = true;
            state.text = ok;
        }, async (error, state) => {
            state.loggedIn = false;
            state.text = error;
        });
    }

    async register() {
        await this.handleFetch("http://localhost:3031/users/register", {
            method: 'POST',
            credentials: 'include',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(this.state.login)
        }, async (ok, state) => {
            state.text = ok;
        }, async (error, state) => {
            state.text = error;
        });
    }

    async logout() {
        await this.handleFetch("http://localhost:3031/users/logout", {
            method: 'POST',
            credentials: 'include',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(this.state.login)
        }, async (ok, state) => {
            state.text = ok;
            state.loggedIn = false;
        }, async (error, state) => {
            state.text = error;
        });
    }


    render(props: StartProps, state: StartState) {
        return <div>
            <div class="container start">
                <div class="background"></div>
                <header class="header">
                    <h1>Welcome to <span class="strong">Cosmic Shopkeeper</span></h1>
                </header>
                <nav>
                    {!state.loggedIn ?
                        <form>
                            <input type="text" name="username"
                                   onInput={(e) => this.updateUsername(e)}
                                   value={this.state.login.username}/>
                            <input type="password" name="password"
                                   onInput={(e) => this.updatePassword(e)}
                                   value={this.state.login.password}/>
                            <button type="button" class="btn"
                                    onClick={(e) => this.login()}>Login
                            </button>
                            <button type="button" class="btn"
                                    onClick={(e) => this.register()}>Register
                            </button>
                        </form> :
                        <form>
                            <button type="button" class="btn"
                                    onClick={(e) => this.logout()}>Logout
                            </button>
                        </form>
                    }
                    <p>{state.text}</p>
                    {state.loggedIn ?
                        <form>
                            <h3>Preset upload</h3>
                            <p>Name:</p>
                            <input type="text" name="presetName"
                                   onInput={(e) => this.updatePresetName(e)}
                                   value={this.state.preset.name}/>
                            <p>Description:</p>
                            <input type="text" name="presetDescription"
                                   onInput={(e) => this.updatePresetDescription(e)}
                                   value={this.state.preset.description}/>
                            <p>File:</p>
                            <input class="btn" type="file" onChange={(e) => this.selectFile(e)}/>
                            <button type="button" class="btn"
                                    onClick={(e) => this.submitPreset()}>Upload!
                            </button>
                        </form> : null
                    }
                </nav>

                <main class="main">
                    <a class="btn" href="#name-popup">Play!</a>
                    <p>Best Scores:</p>
                    <table>
                        <tr>
                            <th>Rank</th>
                            <th>Player</th>
                            <th>Score</th>
                        </tr>
                        {Object.entries(state.scores)
                            .sort(([n1, s1], [n2, s2]) => s2 - s1)
                            .slice(0, 10)
                            .map(([name, score], pos) =>
                                <tr>
                                    <td>{pos + 1}</td>
                                    <td>{name}</td>
                                    <td>{score}</td>
                                </tr>
                            )}
                    </table>
                </main>

                <footer class="footer">
                    <p>Thank u!</p>
                    <p>Copyright 2019 Van Binh Studios</p>
                </footer>
            </div>

            <div class="popup" id="name-popup">
                <a class="close" href="#">&times;</a>
                <form>
                    <h2>How shall we call ye, Shopkeeper?</h2>
                    <input autofocus class="btn" name="name" type="text"
                           onInput={(e) => this.updatePlayerName(e)}
                           value={this.state.playerName}/>

                    <select class="btn"
                            value={state.selectedPreset == null ? "" : state.selectedPreset.name}
                            onChange={(e) => this.changeSelectedPreset(e)}>
                        <option value="">Default</option>
                        {state.presetDetails.map(details => {
                            return <option value={details.name}>{details.name}</option>
                        })}
                    </select>
                    {state.selectedPreset &&
                    <div>
                        <p class="strong">Preset details:</p>
                        <p><span class="strong">Name: </span> {state.selectedPreset.name}</p>
                        <p><span class="strong">Author: </span> {state.selectedPreset.author}</p>
                        <p><span class="strong">Created: </span> {new Date(state.selectedPreset.created).toDateString()}
                        </p>
                        <p><span class="strong">Desription: </span> {state.selectedPreset.description}</p>
                    </div>
                    }

                    <button class="btn" value="Onwards!" onClick={(e) =>
                        this.play()
                    }>Play!
                    </button>
                </form>
            </div>
            <a class="close-popup" href="#"></a>
        </div>

    }
}